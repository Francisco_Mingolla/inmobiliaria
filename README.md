# Caso de Estudio : “Inmobiliaria“

## Se pretende un producto de software que permita administrar las operaciones que se llevan a cabo en una inmobiliaria, las funciones/operaciones mínimas principales requeridas son:

- [ ] Incorporar una propiedad para la venta o alquiler, donde las propiedades se pueden clasificar en: casas, departamentos, locales comerciales (pueden surgir más clasificaciones en el futuro). Las propiedades tienen como datos mínimos: los metros cuadrados cubiertos y la ubicación (localidad, barrio y dirección), en el caso de los departamentos el piso y si tiene cochera, en el caso de los locales comerciales si tiene baño y/o cocina y metros cuadrados de vidriera. En las casas y departamentos se tiene el dato de la cantidad de ambientes y el tipo de ambientes (dormitorio, baño, cocina, lavadero, comedor, cocina-comedor, etc).
- [ ] Las operaciones comerciales que se deben proveer son: Vender o Alquilar una propiedad en particular. El alquiler a su vez puede ser temporal en días (solo para casas y departamentos) o Mensual. En el alquiler temporal aparte del precio por día tiene la opción de un precio opcional por toallas y sabanas. En el caso del alquiler Mensual se debe especificar la cantidad de meses.
- [ ] Por cada operación comercial de venta/alquiler se debe emitir un comprobante de factura. El sistema debe tener una opción para obtener la información de la facturación tanto por día o por mes. La factura puede tener más de un ítems en el caso del alquiler temporal (dias de alquiler y si opta por el servicio de toallas y sabanas)
- [ ] El sistema debe gestionar (operaciones de Alta-Baja-Modificación-Consulta) los datos personales de los clientes de la inmobiliaria, tanto de los compradores como de los vendedores.
- [ ] El sistema debe permitir consultas varias donde se pueda obtener información para la toma de decisión de los empleados de la Inmobiliaria. Por ejemplo:
- [ ] Consulta de total de facturación por dia o mes
- [ ] Consulta de las operaciones de facturación realizadas por día o mes.
- [ ] Consulta de propiedades disponibles para facturar según criterios de consulta del interesado (si busca para alquilar o comprar, si quiere alquiler temporario el rango de fecha que desea, etc).
