package inmobiliaria;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Test;

public class FacturadorTest {
    @Test
    public void verificarCargarFacturaVenta(){
        LocalDate fechaActual = LocalDate.now();
        BigDecimal precioVenta = BigDecimal.valueOf(1500.50);
        BigDecimal precioAlquilerMensual = BigDecimal.valueOf(500.50);
        BigDecimal precioAlquilerDiario = BigDecimal.valueOf(50.50);
        // seccion propiedad
        Propiedad propiedad;
        ArrayList<Propiedad> listaPropiedades = new ArrayList<>();
        PadronPropiedades manejoPropiedades = new PadronPropiedades(listaPropiedades);
        ArrayList<Ambiente> listaAmbientes = new ArrayList<>();
        Ambiente ambiente = new Ambiente("baño", 1);
        listaAmbientes.add(ambiente);
        Casa casa = new Casa(20, "capital", "Altos Verdes", "Sal gema 45", precioVenta, precioAlquilerMensual, precioAlquilerDiario, listaAmbientes);
        manejoPropiedades.setPropiedades(casa);
        propiedad=manejoPropiedades.buscarPropiedad(casa);
        // seccion cliente
        Cliente cliente;
        ArrayList<Cliente> listaClientes = new ArrayList<>();
        PadronClientes manejoCliente = new PadronClientes(listaClientes);
        cliente=manejoCliente.setCliente("Francisco", 42843636, "sur",2);
        //seccion factura
        Factura factura;
        ArrayList<Factura> listaFacturas = new ArrayList<>();
        Facturador facturador = new Facturador(listaFacturas, manejoPropiedades, manejoCliente);
        factura=facturador.cargarFacturaVenta(propiedad, cliente,fechaActual);
        // comprobar factura
        LocalDate fechaAlquiler = LocalDate.of(1, 1, 1);
        BigDecimal precioOpcional=BigDecimal.valueOf(0.00);
        Factura comprobarFactura = new Factura(cliente, propiedad, propiedad.getPrecioVenta(), precioOpcional, fechaAlquiler, fechaAlquiler, fechaActual);
        factura.equals(comprobarFactura);
        // comprobar precio
        assertEquals(precioVenta, propiedad.getPrecioVenta());
        //comprobar lista de facturas
        assertEquals(1, facturador.getNominaFacturas().size());
        // comprobar propiedad vendida luego de la facturacion
        assertEquals(0, manejoPropiedades.getNominaPropiedades().size());
    }
    @Test
    public void verificarTotalFacturacines(){
        LocalDate fechaActual = LocalDate.now();
        BigDecimal precioVenta = BigDecimal.valueOf(1500.50);
        BigDecimal precioAlquilerMensual = BigDecimal.valueOf(500.50);
        BigDecimal precioAlquilerDiario = BigDecimal.valueOf(50.50);
        // seccion propiedad
        Propiedad propiedad;
        ArrayList<Propiedad> listaPropiedades = new ArrayList<>();
        PadronPropiedades manejoPropiedades = new PadronPropiedades(listaPropiedades);
        ArrayList<Ambiente> listaAmbientes = new ArrayList<>();
        Ambiente ambiente = new Ambiente("baño", 1);
        listaAmbientes.add(ambiente);
        Casa casa = new Casa(20, "capital", "Altos Verdes", "Sal gema 45", precioVenta, precioAlquilerMensual, precioAlquilerDiario, listaAmbientes);
        manejoPropiedades.setPropiedades(casa);
        propiedad=manejoPropiedades.buscarPropiedad(casa);
        // seccion cliente
        Cliente cliente;
        ArrayList<Cliente> listaClientes = new ArrayList<>();
        PadronClientes manejoCliente = new PadronClientes(listaClientes);
        cliente=manejoCliente.setCliente("Francisco", 42843636, "sur",2);
        //seccion factura
        ArrayList<Factura> listaFacturas = new ArrayList<>();
        ArrayList<Factura> listaFacturasxFechas;
        Facturador facturador = new Facturador(listaFacturas, manejoPropiedades, manejoCliente);
        facturador.cargarFacturaVenta(propiedad, cliente,fechaActual);
        //compurebo de facturacion mensual
        listaFacturasxFechas=facturador.getTotalFacturas(fechaActual, 1);
        assertEquals(1, listaFacturasxFechas.size());
        //compurebo de facturacion diario
        listaFacturasxFechas=facturador.getTotalFacturas(fechaActual, 2);
        assertEquals(1, listaFacturasxFechas.size());
        //compruebo total facturado
        BigDecimal comprobarFacturado;
        comprobarFacturado=facturador.calcularFacturado(listaFacturasxFechas);
        comprobarFacturado.equals(precioVenta);
        // compruebo cantidad de operaciones
        Integer comprobarOperaciones=1;
        assertEquals(comprobarOperaciones, facturador.calcularOperaciones(listaFacturasxFechas));
    }
    @Test
    public void verificarCargarFacturaAlquiler(){
        LocalDate fechaActual = LocalDate.now();
        BigDecimal precioVenta = BigDecimal.valueOf(1500.50);
        BigDecimal precioAlquilerMensual = BigDecimal.valueOf(500.50);
        BigDecimal precioAlquilerDiario = BigDecimal.valueOf(50.50);
        BigDecimal precioOpcional = BigDecimal.valueOf(5.50);
        // seccion propiedad
        Propiedad propiedad;
        ArrayList<Propiedad> listaPropiedades = new ArrayList<>();
        PadronPropiedades manejoPropiedades = new PadronPropiedades(listaPropiedades);
        ArrayList<Ambiente> listaAmbientes = new ArrayList<>();
        Ambiente ambiente = new Ambiente("baño", 1);
        listaAmbientes.add(ambiente);
        Casa casa = new Casa(20, "capital", "Altos Verdes", "Sal gema 45", precioVenta, precioAlquilerMensual, precioAlquilerDiario, listaAmbientes);
        manejoPropiedades.setPropiedades(casa);
        propiedad=manejoPropiedades.buscarPropiedad(casa);
        // seccion cliente
        Cliente cliente;
        ArrayList<Cliente> listaClientes = new ArrayList<>();
        PadronClientes manejoCliente = new PadronClientes(listaClientes);
        cliente=manejoCliente.setCliente("Francisco", 42843636, "sur",2);
        //seccion factura
        Factura factura;
        ArrayList<Factura> listaFacturas = new ArrayList<>();
        Facturador facturador = new Facturador(listaFacturas, manejoPropiedades, manejoCliente);
        LocalDate fechaInicio = LocalDate.of(2022, 3, 5);
        LocalDate fechaFin = LocalDate.of(2022, 3, 8);
        factura=facturador.cargarFacturaAlquiler(propiedad, cliente, fechaActual, fechaInicio, fechaFin, precioOpcional);
        // comprobar factura
        Factura comprobarFactura = new Factura(cliente, propiedad, propiedad.getPrecioVenta(), precioOpcional, fechaInicio, fechaFin, fechaActual);
        factura.equals(comprobarFactura);
        //comprobar lista de facturas
        assertEquals(1, facturador.getNominaFacturas().size());
        // comprobar propiedad vendida luego de la facturacion
        assertEquals(0, manejoPropiedades.getNominaPropiedades().size());
    }
}
