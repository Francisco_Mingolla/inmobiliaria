package inmobiliaria;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;
import java.math.BigDecimal;
public class DepartamentoTest {
    @Test
    public void verificarRetornoToString(){
        BigDecimal precio = BigDecimal.valueOf(1500.50);
        ArrayList<Ambiente> listaAmbientes = new ArrayList<Ambiente>();
        Ambiente ambiente = new Ambiente("cocina", 2);
        listaAmbientes.add(ambiente);
        ambiente = new Ambiente("baño", 2);
        listaAmbientes.add(ambiente);
        Departamento departemento = new Departamento(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, 1, true, listaAmbientes, precio);
        String mensaje = departemento.toString();
        assertEquals("true", mensaje);
    }
}
