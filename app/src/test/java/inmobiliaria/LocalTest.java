package inmobiliaria;

import java.math.BigDecimal;
import org.junit.Test;
import static org.junit.Assert.*;

public class LocalTest {
    @Test
    public void verificarRetornoToString(){
        BigDecimal precio = BigDecimal.valueOf(1500.50);
        Local local = new Local(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, 1, 1, 1, 5);
        String mensaje = local.toString();
        assertEquals("true", mensaje);
    }
}
