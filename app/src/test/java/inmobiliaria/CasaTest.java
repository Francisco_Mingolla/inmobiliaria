package inmobiliaria;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;
import java.math.BigDecimal;
public class CasaTest {
    @Test
    public void verificarRetornoToString(){
        BigDecimal precioVenta = BigDecimal.valueOf(15000.50);
        BigDecimal precioAlquilerMensual = BigDecimal.valueOf(500.50);
        BigDecimal precioAlquilerDiario = BigDecimal.valueOf(50.50);
        ArrayList<Ambiente> listaAmbientes = new ArrayList<Ambiente>();
        Ambiente ambiente = new Ambiente("cocina", 2);
        listaAmbientes.add(ambiente);
        ambiente = new Ambiente("baño", 1);
        listaAmbientes.add(ambiente);
        Casa casa = new Casa(20, "capital", "Altos Verdes", "Sal gema 45", precioVenta, precioAlquilerMensual, precioAlquilerDiario, listaAmbientes);
        String mensaje = casa.toString();
        assertEquals("true", mensaje);
    }
}
