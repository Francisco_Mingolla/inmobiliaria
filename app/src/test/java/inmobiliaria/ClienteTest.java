package inmobiliaria;

import org.junit.Test;
import static org.junit.Assert.*;

public class ClienteTest {
    @Test
    public void verificarRetornoToString(){
        Cliente cliente = new Cliente("Francisco", 42843636, "Altos verdes");
        String mensaje = cliente.toStringVerificar();
        assertEquals("Cliente cargado", mensaje);
    }
    @Test
    public void verificarSetTipoCliente(){
        Cliente cliente = new Cliente("Francisco", 42843636, "Altos verdes");
        cliente.setTipoCliente(true);
        assertEquals("Comprador", cliente.getTipoCliente());
    }
}
