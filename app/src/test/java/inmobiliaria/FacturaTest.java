package inmobiliaria;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

public class FacturaTest {
    @Test
    public void verificarRetornoToString(){
        BigDecimal precio = BigDecimal.valueOf(1500.50);
        BigDecimal precioTotal = BigDecimal.valueOf(1500.50);
        BigDecimal precioOpcional = BigDecimal.valueOf(1500.50);
        Cliente cliente = new Cliente("Francisco", 42843636, "Altos verdes");
        Local local = new Local(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, 1, 1, 1, 5);
        LocalDate fechaAlquilerInicio = LocalDate.of(2022, 10, 15);
        LocalDate fechaAlquilerFin = LocalDate.of(2022, 10, 30);
        LocalDate fechaActual = LocalDate.now();
        Factura factura = new Factura(cliente, local, precioTotal, precioOpcional, fechaAlquilerInicio, fechaAlquilerFin, fechaActual);
        String mensaje = factura.toString();
        assertEquals("true", mensaje);
    }
    @Test
    public void verificarFechaAlquileres(){
        // seccion casa
        BigDecimal precioVenta = BigDecimal.valueOf(15000.50);
        BigDecimal precioAlquilerMensual = BigDecimal.valueOf(500.50);
        BigDecimal precioAlquilerDiario = BigDecimal.valueOf(50.50);
        BigDecimal precioOpcional = BigDecimal.valueOf(5.00);
        // seccion ambientes
        ArrayList<Ambiente> listaAmbientes = new ArrayList<Ambiente>();
        Ambiente ambiente = new Ambiente("cocina", 2);
        listaAmbientes.add(ambiente);
        ambiente = new Ambiente("baño", 1);
        listaAmbientes.add(ambiente);
        Casa casa = new Casa(20, "capital", "Altos Verdes", "Sal gema 45", precioVenta, precioAlquilerMensual, precioAlquilerDiario, listaAmbientes);
        // seccion cliente y fechas
        Cliente cliente = new Cliente("Francisco", 42843636, "Altos verdes");
        LocalDate fechaAlquilerInicio = LocalDate.of(2022, 5, 15);
        LocalDate fechaAlquilerFin = LocalDate.of(2022, 7, 20);
        LocalDate fechaActual = LocalDate.now();
        // factura
        Factura factura = new Factura(cliente, casa, precioVenta, precioOpcional, fechaAlquilerInicio, fechaAlquilerFin, fechaActual);
        // variables fechas y precios a comprobar
        Long diasDeDiferencia = (long) 5;
        Long meseesDeDiferencia = (long)2;
        double precioTotal;
        precioTotal=casa.getPrecioAlquilerMensual().longValue()*meseesDeDiferencia;
        precioTotal+=casa.getPrecioAlquilerDiario().longValue()*diasDeDiferencia;
        BigDecimal precioTotaldeAlquiler = BigDecimal.valueOf(precioTotal);
        // combruebo fechas
        assertEquals(diasDeDiferencia, factura.calcularFechaAlquilerDias());
        assertEquals(meseesDeDiferencia, factura.calcularFechaAlquilerMensual());
        // combruebo precios
        assertEquals(precioTotaldeAlquiler, factura.calcularPrecioAlquiler());
        String mensaje = factura.toString();
        assertEquals("true", mensaje);
    }
}
