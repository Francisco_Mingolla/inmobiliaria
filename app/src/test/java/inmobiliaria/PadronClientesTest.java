package inmobiliaria;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

public class PadronClientesTest {
    Cliente cliente;
    ArrayList<Cliente> listaClientes = new ArrayList<>();
    PadronClientes manejoCliente = new PadronClientes(listaClientes);
    @Test
    public void verificarRetornoToString(){
        String mensaje = manejoCliente.toStringVerificar();
        assertEquals("true", mensaje);
    }
    @Test
    public void verificarSetCliente(){
        String nombre = "Francisco";
        Integer dni =  42843636;
        String direccion = "Altos verdes";
        cliente = new Cliente(nombre, dni, direccion);
        Cliente comprobarCliente = manejoCliente.setCliente(nombre, dni, direccion,1);
        assertEquals(cliente.getDni(), comprobarCliente.getDni());
    }
    @Test
    public void verificarSetClientes(){
        cliente = new Cliente("Fran", 42843639, "norte");
        manejoCliente.setCliente("Francisco", 42843636, "sur",1);
        manejoCliente.setCliente("Fran", 42843639, "norte",1);
        assertEquals(2, manejoCliente.getNominaClientes().size());
    }
    @Test
    public void verificarBuscarCliente(){
        Integer dni=42843636;
        cliente=manejoCliente.setCliente("Francisco", dni, "sur",1);
        Cliente comprobarCliente;
        comprobarCliente=manejoCliente.buscarCliente(dni);
        cliente.equals(comprobarCliente);
    }
    @Test
    public void verificarEliminarCliente(){
        cliente=manejoCliente.setCliente("Francisco", 42843636, "sur",2);
        manejoCliente.eliminarCliente(cliente);
        assertEquals(0, manejoCliente.getNominaClientes().size());
    }
    @Test
    public void verificarModificarCliente(){
        cliente=manejoCliente.setCliente("Francisco", 42843636, "sur",2);
        manejoCliente.modificarCliente(cliente, "Fran", "norte", 12345,2);
        assertEquals("Fran", manejoCliente.buscarCliente(12345).getNombre());
    }
}
