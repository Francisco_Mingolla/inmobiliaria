package inmobiliaria;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.util.ArrayList;

public class PadronPropiedadesTest {
    ArrayList<Ambiente> listaAmbientes;
    Cliente cliente;
    Ambiente ambiente;
    Casa casa;
    Local local;
    Departamento departamento;
    BigDecimal precio = BigDecimal.valueOf(1500.50);
    // seccion propiedades
    @Test
    public void verificarSetPropiedades(){
        ArrayList<Propiedad> listaPropiedades = new ArrayList<>();
        PadronPropiedades manejoPropiedades = new PadronPropiedades(listaPropiedades);
        listaAmbientes = new ArrayList<>();
        ambiente = new Ambiente("baño", 1);
        listaAmbientes.add(ambiente);
        casa = new Casa(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, precio, listaAmbientes);
        manejoPropiedades.setPropiedades(casa);
        departamento = new Departamento(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, 1, true, listaAmbientes, precio);
        manejoPropiedades.setPropiedades(departamento);
        assertEquals(2,manejoPropiedades.getNominaPropiedades().size());
    }
    @Test
    public void verificarBuscarPropiedad(){
        ArrayList<Propiedad> listaPropiedades = new ArrayList<>();
        PadronPropiedades manejoPropiedades = new PadronPropiedades(listaPropiedades);
        Propiedad buscaPropiedad;
        local = new Local(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, 1, 1, 1, 5);
        buscaPropiedad = local;
        manejoPropiedades.setPropiedades(local);
        assertEquals(local, manejoPropiedades.buscarPropiedad(buscaPropiedad));
    }
    @Test
    public void verificarEliminarPropiedad(){
        ArrayList<Propiedad> listaPropiedades = new ArrayList<>();
        PadronPropiedades manejoPropiedades = new PadronPropiedades(listaPropiedades);
        local = new Local(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, 1, 1, 1, 5);
        manejoPropiedades.setPropiedades(local);
        manejoPropiedades.eliminarPropiedad(local);
        assertEquals(0, manejoPropiedades.getNominaPropiedades().size()); 
    }
    @Test
    public void verificarcomprobarPropiedadAVender(){
        ArrayList<Propiedad> listaPropiedades = new ArrayList<>();
        PadronPropiedades manejoPropiedades = new PadronPropiedades(listaPropiedades);
        Propiedad comprobarPropiedad;
        listaAmbientes = new ArrayList<>();
        ambiente = new Ambiente("cocina", 2);
        listaAmbientes.add(ambiente);
        ambiente = new Ambiente("baño", 1);
        listaAmbientes.add(ambiente);
        casa = new Casa(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, precio, listaAmbientes);
        manejoPropiedades.setPropiedades(casa);
        comprobarPropiedad = manejoPropiedades.comprobarPropiedadAVender(casa);
        assertEquals(casa, comprobarPropiedad);
        //forma de lanzar la excepcion
        //local = new Local(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, 1, 1, 1, 5);
        //manejoPropiedades.setPropiedades(local);
        //comprobarPropiedad = manejoPropiedades.comprobarPropiedadAVender(local);
        //assertEquals(null, comprobarPropiedad);
    }
    // seccion casa
    @Test
    public void verificarSetCasa(){
        ArrayList<Propiedad> listaPropiedades = new ArrayList<>();
        PadronPropiedades manejoPropiedades = new PadronPropiedades(listaPropiedades);
        listaAmbientes = new ArrayList<Ambiente>();
        Casa comprobarCasa;
        ambiente = new Ambiente("cocina", 2);
        listaAmbientes.add(ambiente);
        ambiente = new Ambiente("baño", 1);
        listaAmbientes.add(ambiente);
        casa = new Casa(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, precio, listaAmbientes);
        comprobarCasa = manejoPropiedades.setCasa(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, precio, listaAmbientes);
        // assertEquals(casa, comprobarCasa);
        casa.equals(comprobarCasa);
    }
    // seccion ambientes
    @Test
    public void verificarSetAmbiente(){
        ArrayList<Propiedad> listaPropiedades = new ArrayList<>();
        PadronPropiedades manejoPropiedades = new PadronPropiedades(listaPropiedades);
        ambiente = new Ambiente("cocina", 2);
        Ambiente comprobarAmbiente = manejoPropiedades.setAmbiente("cocina", 2);
        // assertEquals(ambiente, comprobarAmbiente);
        ambiente.equals(comprobarAmbiente);
    }
    @Test
    public void verificarSetListaAmbientes(){
        ArrayList<Propiedad> listaPropiedades = new ArrayList<>();
        PadronPropiedades manejoPropiedades = new PadronPropiedades(listaPropiedades);
        listaAmbientes = new ArrayList<>();
        ArrayList<Ambiente> comprobarListaAmbientes;
        ambiente = new Ambiente("cocina", 2);
        comprobarListaAmbientes = manejoPropiedades.setListaAmbientes(listaAmbientes, ambiente);
        // observar
        assertEquals(1, comprobarListaAmbientes.size());
    }
    // seccion departamento
    @Test
    public void verificarSetDepartamento(){
        ArrayList<Propiedad> listaPropiedades = new ArrayList<>();
        PadronPropiedades manejoPropiedades = new PadronPropiedades(listaPropiedades);
        listaAmbientes = new ArrayList<Ambiente>();
        Departamento comprobarDepartamento;
        ambiente = new Ambiente("cocina", 2);
        listaAmbientes.add(ambiente);
        ambiente = new Ambiente("baño", 1);
        listaAmbientes.add(ambiente);
        departamento = new Departamento(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, 1, true, listaAmbientes, precio);
        comprobarDepartamento = manejoPropiedades.setDepartamento(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, 1, true, listaAmbientes, precio);
        // assertEquals(departamento, comprobarDepartamento);
        departamento.equals(comprobarDepartamento);
    }
    // seccion local
    @Test
    public void verificarSetLocal(){
        ArrayList<Propiedad> listaPropiedades = new ArrayList<>();
        PadronPropiedades manejoPropiedades = new PadronPropiedades(listaPropiedades);
        Local comprobarLocal;
        local = new Local(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, 1, 1, 1, 5);
        comprobarLocal = manejoPropiedades.setLocal(20, "capital", "Altos Verdes", "Sal gema 45", precio, precio, 1, 1, 1, 5);
        // assertEquals(local, comprobarLocal);
        local.equals(comprobarLocal);
    }
}
