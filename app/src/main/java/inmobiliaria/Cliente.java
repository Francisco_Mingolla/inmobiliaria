package inmobiliaria;

public class Cliente {
    // atributos
    private  String nombre;
    private Integer dni;
    private String direccion;
    private String tipoCliente;
    // constructor para test
    public Cliente(String nombre, Integer dni, String direccion){
        this.nombre=nombre;
        this.dni=dni;
        this.direccion=direccion;
    }
    // public void mostrarCliente(){
    //     System.out.println("Nombre: " + nombre);
    //     System.out.println("Dni: " + dni);
    //     System.out.println("Direccion: " + direccion);
    // }
    // sets
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    public void setDni(Integer dni){
        this.dni=dni;
    }
    public void setDireccion(String direccion){
        this.direccion=direccion;
    }
    public void setTipoCliente(boolean tipo){
        if(tipo==true)
            tipoCliente="Comprador";
        else
            tipoCliente="Vendedor";
    }
    // gets
    public String getNombre(){
        return nombre;
    }
    public String getDireccion(){
        return direccion;
    }
    public String getTipoCliente(){
        return tipoCliente;
    }
    public Integer getDni(){
        return dni;
    }
    // test
    public String toStringVerificar(){
        return "Cliente cargado";
    }
}
