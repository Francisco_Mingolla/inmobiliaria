package inmobiliaria;

public class ClienteInexistenteException extends RuntimeException{
    public ClienteInexistenteException(){
        super("Cliente Inexistente");
    }
}
