package inmobiliaria;
import java.math.BigDecimal;
import java.util.ArrayList;

public class Casa extends Propiedad {
    // atributos
    Ambiente ambiente;
    ArrayList<Ambiente> listaAmbientes;
    private BigDecimal precioAlquilerDia;
    // constructor para test
    public Casa(Integer metros2, String localidad, String barrio, String direccion, BigDecimal precioVenta, BigDecimal precioAlquilerMensual, BigDecimal precioAlquilerDia, ArrayList<Ambiente> listaAmbientes){
        super(metros2, localidad, barrio, direccion, precioVenta, precioAlquilerMensual);
        this.listaAmbientes=listaAmbientes;
        this.precioAlquilerDia=precioAlquilerDia;
    }
    // metodos
    public void mostrarAmbientes(){
        for (Ambiente ambiente : listaAmbientes) {
            System.out.println("Tipo de Ambiente: " + ambiente.getTipo() + "Cantidad: " + ambiente.getCantidad());
        }
    }
    public BigDecimal getPrecioAlquilerDiario(){
        return precioAlquilerDia;
    }
    // test
    public String toString(){
        return "true";
    }
}
