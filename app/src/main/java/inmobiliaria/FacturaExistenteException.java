package inmobiliaria;

public class FacturaExistenteException extends RuntimeException{
    public FacturaExistenteException(){
        super("La factura ya existe");
    }
}
