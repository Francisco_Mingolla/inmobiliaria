package inmobiliaria;

public class PropiedadExistenteException extends RuntimeException
{
    public PropiedadExistenteException() {
        super("La propiedad ya exite en la base de datos");
    }
}