package inmobiliaria;

public class FacturaIxistenteException extends RuntimeException{
    public FacturaIxistenteException(){
        super("Factura Inexistente");
    }
}
