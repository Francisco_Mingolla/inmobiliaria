package inmobiliaria;

public class PropiedadInexistenteException extends RuntimeException{
    public PropiedadInexistenteException() {
        super("No se encontro la propiedad o propiedades");
    }
}
