package inmobiliaria;

import java.util.*;

public class Ambiente {
    // atributos
    private String tipo;
    private int cantidad;
    Scanner entrada;
    // constructor
    public Ambiente(String tipo, int cantidad){
        this.tipo=tipo;
        this.cantidad=cantidad;
    }
    // gets
    public String getTipo(){
        return tipo;
    }
    public int getCantidad(){
        return cantidad;
    }
}
