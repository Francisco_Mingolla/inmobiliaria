package inmobiliaria;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;


public class PadronPropiedades {
    private ArrayList<Propiedad> listaPropiedades;
    private Casa casa;
    private Local local;
    private Departamento departamento;
    private Scanner entrada;
    public PadronPropiedades(ArrayList<Propiedad> listaPropiedades){
        this.listaPropiedades=listaPropiedades;
    }
    // seccion propiedad
    public void cargarPropiedad()  throws PropiedadExistenteException {
        // vbles propiedad
        Integer metros2;
        String localidad, barrio, direccion;
        BigDecimal precioVenta, precioAlquilerMensual;
        // vbles entre casa y deptos
        ArrayList<Ambiente> listaAmbientes=new ArrayList<>();
        BigDecimal precioAlquilerDia;
        // vbles depto
        int piso;
        boolean cochera=false;
        // vbles local
        int baños, pisos, cocinas, m2vidriera;
        // vbles de control
        boolean salir=false;
        int respuesta;
        entrada = new Scanner(System.in);
        System.out.println("Complete la informacion de la propiedad");
        System.out.print("Metros cuadrados: ");
        metros2=entrada.nextInt();
        System.out.print("Localidad: ");
        localidad=entrada.nextLine();
        System.out.print("Barrio: ");
        barrio=entrada.nextLine();
        System.out.print("Direccion: ");
        direccion=entrada.nextLine();
        System.out.print("Precio de venta: ");
        precioVenta=entrada.nextBigDecimal();
        System.out.print("Precio de alquiler por mes: ");
        precioAlquilerMensual=entrada.nextBigDecimal();
        System.out.println("Ingrese el tipo de propiedad: ");
        System.out.println("1: Casa");
        System.out.println("2: Departamento");
        System.out.println("3: Local comercial");
        System.out.print("Respuesta numerica: ");
        respuesta=entrada.nextInt();
        do {
            switch (respuesta) {
                case 1:
                    System.out.println("Complete la informacion de la casa");
                    System.out.print("Precio de alquiler por dia: ");
                    precioAlquilerDia=entrada.nextBigDecimal();
                    cargarAmbientes(listaAmbientes);
                    casa = setCasa(metros2, localidad, barrio, direccion, precioVenta, precioAlquilerMensual, precioAlquilerDia, listaAmbientes);
                    setPropiedades(casa);
                    salir=true;
                    break;
                case 2:
                    System.out.println("Complete la informacion del departamento");
                    System.out.print("Precio de alquiler por dia: ");
                    precioAlquilerDia=entrada.nextBigDecimal();
                    System.out.print("Numero de piso: ");
                    piso=entrada.nextInt();
                    do {
                        System.out.println("Cochera?");
                        System.out.println("1: Si");
                        System.out.println("2: No");
                        System.out.print("Respuesta numerica: ");
                        respuesta=entrada.nextInt();
                        switch (respuesta) {
                            case 1:
                                cochera=true;
                                salir=true;
                                break;
                            case 2:
                                cochera=false;
                                salir=true;
                                break;
                            default:
                                System.out.println("Respuesta incorrecta");
                            break;
                        }
                    } while (salir);
                    cargarAmbientes(listaAmbientes);
                    departamento = setDepartamento(metros2, localidad, barrio, direccion, precioVenta, precioAlquilerMensual, piso, cochera, listaAmbientes, precioAlquilerDia);
                    setPropiedades(departamento);
                    break;
                case 3:
                    System.out.println("Complete la informacion del local comercial");
                    System.out.print("Cantidad de baños: ");
                    baños=entrada.nextInt();
                    System.out.print("Cantidad de pisos: ");
                    pisos=entrada.nextInt();
                    System.out.print("Cantidad de cocinas: ");
                    cocinas=entrada.nextInt();
                    System.out.print("Metros cuadrados de vidriera: ");
                    m2vidriera=entrada.nextInt();
                    local = setLocal(metros2, localidad, barrio, direccion, precioVenta, precioAlquilerMensual, baños, pisos , cocinas, m2vidriera);
                    setPropiedades(local);
                    salir=true;
                    break;
                default:
                    System.out.println("Respuesta incorrecta");
                    break;
            }
        } while (salir);
    }
    // agrega propiedades
    public void setPropiedades(Propiedad propiedadnueva) throws PropiedadExistenteException {
        for (Propiedad propiedad : listaPropiedades) {
            if (propiedad.equals(propiedadnueva))
            {
                throw new PropiedadExistenteException();
            }
        }
        this.listaPropiedades.add(propiedadnueva);
    }
    // cantidad de propiedades
    public ArrayList<Propiedad> getNominaPropiedades(){
        return listaPropiedades;
    }
    //encontrar propiedad
    public Propiedad buscarPropiedad(Propiedad buscarPropiedad)  throws PropiedadInexistenteException{
        Propiedad propiedadEncontrada = null;
        for (Propiedad propiedad : listaPropiedades) {
            if(propiedad.equals(buscarPropiedad)){
                propiedadEncontrada=propiedad;
                break;
            }
        }
        if(propiedadEncontrada == null){
            throw new PropiedadInexistenteException();
        }
        return propiedadEncontrada;
    }
    //mostrar propiedades
    public void mostrarPropiedades(){
        int contador=1;
        for (Propiedad propiedad : listaPropiedades){
            System.out.println("Lista de propiedades");
            System.out.println("Nro: " + contador);
            contador++;
            propiedad.mostrarPropiedad();
        }
    }
    //eliminar una propiedad
    public void eliminarPropiedad(Propiedad propiedad)  throws PropiedadInexistenteException{
        Propiedad propiedadEncontrada = buscarPropiedad(propiedad);
        this.listaPropiedades.remove(propiedadEncontrada);
    }
    //seleccionar propiedad
    public Propiedad seleccionarPropiedad() throws PropiedadInexistenteException{
        int numeroPropiedad;
        entrada = new Scanner(System.in);
        Propiedad propiedadSeleccionada = null;
        mostrarPropiedades();
        System.out.println("Ingrese el numero de la propiedad como aparece en la lista");
        System.out.print("Numero de la propiedad: ");
        numeroPropiedad=entrada.nextInt();
        propiedadSeleccionada=listaPropiedades.get(numeroPropiedad);
        // busca la propiedad , genera excepcion de lo contrario
        propiedadSeleccionada=buscarPropiedad(propiedadSeleccionada);
        return propiedadSeleccionada;
    }
    //comprueba que la propiedad sea casa o departamento, no se puede vender un local
    public Propiedad comprobarPropiedadAVender(Propiedad propiedad) throws PropiedadNoVendibleException{
        Propiedad comprobarPropiedad=null;
        if((propiedad instanceof Departamento)||(propiedad instanceof Casa))
        {
            comprobarPropiedad=propiedad;
        }
        if(comprobarPropiedad==null)
        {
            throw new PropiedadNoVendibleException();
        }
        return comprobarPropiedad;
    }
    // seccion ambiente
    public void cargarAmbientes(ArrayList<Ambiente> listaAmbientes){
        entrada = new Scanner(System.in);
        Ambiente ambiente;
        boolean salir=true;
        String tipoAmbiente;
        int cantidadAmbiente,respuesta;
        do {
            System.out.println("Complete los ambientes");
            System.out.print("Tipo de ambiente: ");
            tipoAmbiente = entrada.nextLine();
            System.out.print("Cantidad de ambiente " + tipoAmbiente + " : ");
            cantidadAmbiente = entrada.nextInt();
            ambiente = setAmbiente(tipoAmbiente, cantidadAmbiente);
            setListaAmbientes(listaAmbientes, ambiente);
            System.out.println("Desea agregar otro ambiente ?");
            System.out.print("1: Si");
            System.out.print("2: No");
            System.out.print("Respuesta numerica: ");
            respuesta = entrada.nextInt();
            switch (respuesta) {
                case 1:
                    salir=false;
                    break;
                case 2:
                    salir = true;
                    break;
                default:
                    System.out.println("Respuesta incorrecta");
                    salir=true;
                    break;
            }
        } while (salir);
    }
    public Ambiente setAmbiente(String tipoAmbiente, int cantidadAmbiente){
        Ambiente ambiente = new Ambiente(tipoAmbiente, cantidadAmbiente);
        return ambiente;
    }
    public ArrayList<Ambiente> setListaAmbientes(ArrayList<Ambiente> listaAmbientes,Ambiente ambiente){
        listaAmbientes.add(ambiente);
        return listaAmbientes;
    }
    // seccion casa
    public Casa setCasa(Integer metros2, String localidad, String barrio, String direccion, BigDecimal precioVenta, BigDecimal precioAlquilerMensual, BigDecimal precioAlquilerDia, ArrayList<Ambiente> listaAmbientes){
        casa = new Casa(metros2, localidad, barrio, direccion, precioVenta, precioAlquilerMensual, precioAlquilerDia, listaAmbientes);
        setPropiedades(casa);
        return casa;
    }
    // seccion departamento
    public Departamento setDepartamento(Integer metros2, String localidad, String barrio, String direccion, BigDecimal precioVenta, BigDecimal precioAlquilerMensual, int piso, boolean cochera, ArrayList<Ambiente> listaAmbientes, BigDecimal precioAlquilerDia){
        departamento = new Departamento(metros2, localidad, barrio, direccion, precioVenta, precioAlquilerMensual, piso, cochera, listaAmbientes, precioAlquilerDia);
        setPropiedades(departamento);
        return departamento;
    }
    // seccion local
    public Local setLocal(Integer metros2, String localidad, String barrio, String direccion, BigDecimal precioVenta, BigDecimal precioAlquilerMensual, int baños, int pisos, int cocinas, int m2vidriera){
        local = new Local(metros2, localidad, barrio, direccion, precioVenta, precioAlquilerMensual, baños, pisos, cocinas, m2vidriera);
        setPropiedades(local);
        return local;
    }
    // test
    public String toStringVerificar(){
        return "true";
    }
}
