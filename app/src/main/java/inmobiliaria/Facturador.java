package inmobiliaria;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;


public class Facturador {
    private Factura factura;
    private ArrayList<Factura> listaFacturas;
    private PadronClientes padronClientes;
    private PadronPropiedades padronPropiedades;
    private Scanner entrada;
    public Facturador(ArrayList<Factura> listaFacturas, PadronPropiedades padronPropiedades, PadronClientes padronClientes){
        this.listaFacturas=listaFacturas;
        this.padronClientes=padronClientes;
        this.padronPropiedades=padronPropiedades;
    }
    // creo factura de venta
    public Factura cargarFacturaVenta(Propiedad propiedad, Cliente cliente, LocalDate fechaActual)throws FacturaExistenteException,PropiedadInexistenteException{
        LocalDate fechaAlquiler = LocalDate.of(1, 1, 1);
        BigDecimal precioOpcional=BigDecimal.valueOf(0.00);
        factura = new Factura(cliente, propiedad, propiedad.getPrecioVenta(), precioOpcional, fechaAlquiler, fechaAlquiler, fechaActual);
        setFacturas(factura);
        padronPropiedades.eliminarPropiedad(propiedad);
        return factura;
    }
    // creo factura de alquiler
    public Factura cargarFacturaAlquiler(Propiedad propiedad, Cliente cliente, LocalDate fechaActual, LocalDate fechaInicio, LocalDate fechaFinal, BigDecimal precioOpcional)throws FacturaExistenteException,PropiedadInexistenteException{
        factura = new Factura(cliente, propiedad, propiedad.getPrecioVenta(), precioOpcional, fechaInicio, fechaFinal, fechaActual);
        setFacturas(factura);
        padronPropiedades.eliminarPropiedad(propiedad);
        return factura;
    }
    // agrego factura
    public void setFacturas(Factura facturaNueva) throws FacturaExistenteException{
        for (Factura factura : listaFacturas) {
            if (factura.equals(facturaNueva))
            {
                throw new FacturaExistenteException();
            }
        }
        this.listaFacturas.add(factura);
    }
    // lista de facturas 
    public ArrayList<Factura> getNominaFacturas(){
        return listaFacturas;
    }
    // mostrar facturas
    public void mostrarFacturas(ArrayList<Factura> lista_Facturas){
        int contador=1;
        for (Factura factura : lista_Facturas){
            System.out.println("Lista de facturas");
            System.out.println("Nro: " + contador);
            contador++;
            factura.mostrarFactura();
        }
    }
    // buscar total facturado segun el mes o dia
    public ArrayList<Factura> getTotalFacturas(LocalDate fechaSeleccionada, int tipoFechaSelecccion){
        ArrayList<Factura> listaFacturasSeleccionadas = new ArrayList<>();
        switch (tipoFechaSelecccion) {
            case 1:
                // segun el mes
                for (Factura factura : listaFacturas) {

                    if((fechaSeleccionada.getMonth().equals(factura.getFechaComprobante().getMonth())))
                        listaFacturasSeleccionadas.add(factura);
                }
            break;
            case 2:
                // segun el dia
                for (Factura factura : listaFacturas) {
                    if(fechaSeleccionada.equals(factura.getFechaComprobante()))
                        listaFacturasSeleccionadas.add(factura);
                }
            break;
        }
        return listaFacturasSeleccionadas;
    }
    // calcular total facturado 
    public BigDecimal calcularFacturado(ArrayList<Factura> lista_Facturas){
        BigDecimal totalFacturado = new BigDecimal(0);
        for (Factura factura : lista_Facturas) {
            totalFacturado = totalFacturado.add(factura.getCalcularPrecioTotal());
        }
        return totalFacturado;
    }
    // calcular total operaciones
    public Integer calcularOperaciones(ArrayList<Factura> lista_Facturas){
        Integer totalOperaciones=lista_Facturas.size();
        return totalOperaciones;
    }
    //seleccionar fecha y ver el total facturado y la cantidad de operaciones
    public void seleccionarFacturacionTotal(){
        entrada =  new Scanner(System.in);
        LocalDate fechaSeleccionada;
        int dia, mes, año,respuesta;
        Integer totalOperaciones;
        ArrayList<Factura> listaFacturasSeleccionadas;
        BigDecimal totalFacturado = new BigDecimal(0);
        System.out.println("Opciones de ver la facturacion total y la cantidad de operaciones: ");
        System.out.println("1: Facturacion mensual: ");
        System.out.println("2: Factruacion de un dia especifico: ");
        System.out.print("Respuesta Numerica: ");
        respuesta=entrada.nextInt();
        System.out.println("Fecha:");
        switch (respuesta) {
            case 1:
                System.out.print("Mes: ");
                mes=entrada.nextInt();
                System.out.print("Año: ");
                año=entrada.nextInt();
                fechaSeleccionada= LocalDate.of(año, mes, 0);
                listaFacturasSeleccionadas=getTotalFacturas(fechaSeleccionada, 1);
                mostrarFacturas(listaFacturasSeleccionadas);
                totalFacturado.add(calcularFacturado(listaFacturasSeleccionadas));
                totalOperaciones=calcularOperaciones(listaFacturasSeleccionadas);
                System.out.println("Total facturado: "+totalFacturado);
                System.out.println("Total de operaciones: "+totalOperaciones);
            break;
            case 2:
                System.out.print("Dia: ");
                dia=entrada.nextInt();
                System.out.print("Mes: ");
                mes=entrada.nextInt();
                System.out.print("Año: ");
                año=entrada.nextInt();
                fechaSeleccionada= LocalDate.of(año, mes, dia);
                listaFacturasSeleccionadas=getTotalFacturas(fechaSeleccionada, 2);
                mostrarFacturas(listaFacturasSeleccionadas);
                totalFacturado.add(calcularFacturado(listaFacturasSeleccionadas));
                totalOperaciones=calcularOperaciones(listaFacturasSeleccionadas);
                System.out.println("Total facturado: "+totalFacturado);
                System.out.println("Total de operaciones: "+totalOperaciones);
            break;
            default:
                System.out.println("Respuesta incorrecta");
            break;
        }
    }
    // vender propiedad - genero factura 
    public void facturarPropiedad() throws PropiedadInexistenteException,PropiedadNoVendibleException,FacturaExistenteException{
        int diaInicial, mesInicial, añoInicial,diaFinal, mesFinal, añoFinal,respuesta;
        BigDecimal precioOpcional;
        LocalDate fechaActual = LocalDate.now();
        entrada = new Scanner(System.in);
        Propiedad propiedadSeleccionada = null;
        Cliente clienteSeleccionado = null;
        propiedadSeleccionada = padronPropiedades.seleccionarPropiedad();
        if(propiedadSeleccionada!=null)
        {
            propiedadSeleccionada.mostrarPropiedad();
            System.out.println("Seleccione el cliente");
            clienteSeleccionado = padronClientes.seleccionarCliente();
            if(clienteSeleccionado==null)
            {
                System.out.println("Cliente no encontrado");
                System.out.println("Desea agregar el cliente?");
                System.out.println("1: Si");
                System.out.println("2: No");
                System.out.print("Respuesta numerica: ");
                respuesta = entrada.nextInt();
                switch (respuesta) {
                    case 1:
                        clienteSeleccionado = padronClientes.cargarCliente();
                        break;
                    case 2:
                        break;
                    default:
                        System.out.println("Respuesta incorrecta");
                        break;
                }
                if((clienteSeleccionado!=null)&&(propiedadSeleccionada!=null))
                {
                    System.out.println("Opciones para generar la Factura");
                    System.out.println("1: Vender Propiedad");
                    System.out.println("2: Alquilar Propiedad");
                    System.out.print("Respuesta numerica: ");
                    respuesta = entrada.nextInt();
                    switch (respuesta) {
                        case 1:
                            if(propiedadSeleccionada instanceof Local){
                                System.out.println("Propieadad no vendible");
                                throw new PropiedadNoVendibleException();
                            }
                            else{
                                factura=cargarFacturaVenta(propiedadSeleccionada,clienteSeleccionado,fechaActual);
                                factura.mostrarFactura();
                                System.out.println("Propieadad vendida");
                            }
                            break;
                        case 2:
                            System.out.println("Ingrese la fecha de inicio de alquiler");
                            System.out.print("Dia: ");
                            diaInicial=entrada.nextInt();
                            System.out.print("Mes: ");
                            mesInicial=entrada.nextInt();
                            System.out.print("Año: ");
                            añoInicial=entrada.nextInt();
                            LocalDate fechaInicial= LocalDate.of(añoInicial, mesInicial, diaInicial);
                            System.out.println("Ingrese la fecha de fin de alquiler");
                            System.out.print("Dia: ");
                            diaFinal=entrada.nextInt();
                            System.out.print("Mes: ");
                            mesFinal=entrada.nextInt();
                            System.out.print("Año: ");
                            añoFinal=entrada.nextInt();
                            LocalDate fechaFinal= LocalDate.of(añoFinal, mesFinal, diaFinal);
                            System.out.println("Ingrese precio opcional por toallas y sabanas, de lo contrario ingrese 0");
                            System.out.print("precio opcinal: ");
                            precioOpcional=entrada.nextBigDecimal();
                            if(propiedadSeleccionada instanceof Local){
                                if(factura.calcularFechaAlquilerMensual()>0)
                                {
                                    factura=cargarFacturaAlquiler(propiedadSeleccionada,clienteSeleccionado,fechaActual,fechaInicial,fechaFinal,precioOpcional);
                                    factura.mostrarFactura();
                                    System.out.println("Propieadad alquilada");
                                }
                            }
                            break;
                        default:
                            System.out.println("Respuesta incorrecta");
                            break;
                    }                       
                }
            }
        }
    }
}
