package inmobiliaria;
import java.math.BigDecimal;
import java.util.ArrayList;

public class Departamento extends Propiedad{
    // atributos
    private int piso;
    private boolean cochera;
    Ambiente ambiente;
    ArrayList<Ambiente> listaAmbientes;
    private BigDecimal precioAlquilerDia;
    // constructor para test
    public Departamento(Integer metros2, String localidad, String barrio, String direccion, BigDecimal precioVenta, BigDecimal precioAlquilerMensual, int piso, boolean cochera, ArrayList<Ambiente> listaAmbientes, BigDecimal precioAlquilerDia){
        super(metros2, localidad, barrio, direccion, precioVenta, precioAlquilerMensual);
        this.piso=piso;
        this.cochera=cochera;
        this.listaAmbientes=listaAmbientes;
        this.precioAlquilerDia=precioAlquilerDia;
    }
    public void setAmbientes(Ambiente ambiente){
        this.listaAmbientes.add(ambiente);
    }
    public void mostrarAmbientes(){
        for (Ambiente ambiente : listaAmbientes) {
            System.out.println("Tipo de Ambiente: " + ambiente.getTipo() + "Cantidad: " + ambiente.getCantidad());
        }
    }
    public BigDecimal getPrecioAlquilerDiario(){
        return precioAlquilerDia;
    }
    // test
    public String toString(){
        return "true";
    }
}
