package inmobiliaria;

import java.util.ArrayList;
import java.util.Scanner;

public class PadronClientes {
    private Cliente cliente;
    private ArrayList<Cliente> listaClientes;
    private Scanner entrada;
    public PadronClientes(ArrayList<Cliente> listaClientes){
        this.listaClientes=listaClientes;
    }
    // test
    public Cliente cargarCliente(){
        String nombre;
        Integer dni,tipoCliente;
        String direccion;
        entrada = new Scanner(System.in);
        System.out.println("Complete la informacion del cliente");
        System.out.print("Nombre: ");
        nombre=entrada.nextLine();
        System.out.print("Documento: ");
        dni=entrada.nextInt();
        System.out.print("Direccion: ");
        direccion=entrada.nextLine();
        System.out.println("Tipo de cliente: ");
        System.out.println("1 : Comprador: ");
        System.out.println("2 : Vendedor: ");
        System.out.print("Respuesta numerica: ");
        tipoCliente=entrada.nextInt();
        cliente = setCliente(nombre,dni,direccion,tipoCliente);
        return cliente;
    }
    // alta
    // creo cliente
    public Cliente setCliente(String nombre, Integer dni, String direccion, Integer tipoCliente) throws ClienteExistenteException
    {
        cliente = new Cliente(nombre, dni, direccion);
        if(tipoCliente==1)
            cliente.setTipoCliente(true);
        else if(tipoCliente==2)
            cliente.setTipoCliente(false);
        setClientes(cliente);
        return cliente;
    }
    // agrego cliente
    public void setClientes(Cliente clienteNuevo) throws ClienteExistenteException
    {
        for (Cliente cliente : listaClientes) {
            if (cliente.equals(clienteNuevo))
            {
                throw new ClienteExistenteException();
            }
        }
        this.listaClientes.add(cliente);
    }
    // lista de clientes
    public ArrayList<Cliente> getNominaClientes(){
        return listaClientes;
    }
    // mostrar clientes
    // public void mostrarClientes(){
    //     System.out.println("Lista de clientes");
    //     for (Cliente cliente : listaClientes) {
    //         cliente.mostrarCliente();
    //         System.out.println("");
    //     }
    // }
    // seleccionar dni
    public Cliente seleccionarCliente() throws ClienteInexistenteException{
        Integer dni;
        entrada = new Scanner(System.in);
        // mostrarClientes();
        Cliente clienteSeleccionado;
        System.out.println("Ingrese el documento del cliente");
        System.out.print("Ducumento: ");
        dni=entrada.nextInt();
        clienteSeleccionado=buscarCliente(dni);
        return clienteSeleccionado;
    }
    // buscar cliente
    public Cliente buscarCliente(Integer dni) throws ClienteInexistenteException{
        Cliente clienteEncontrado = null;
        for (Cliente cliente : listaClientes) {
            if(cliente.getDni().equals(dni))
                clienteEncontrado=cliente;
        }
        if(clienteEncontrado==null)
            throw new ClienteInexistenteException();
        return clienteEncontrado;
    }
    // baja
    // eliminar cliente
    public void seleccionar_eliminarCliente() throws ClienteInexistenteException{
        Cliente clienteSeleccionado; 
        clienteSeleccionado=seleccionarCliente();
        eliminarCliente(clienteSeleccionado);
    }
    public void eliminarCliente(Cliente cliente){
        listaClientes.remove(cliente);
    }
    // modificacion - falta test
    public void modificarCliente(Cliente cliente, String nombre, String direccion,Integer dni, Integer tipoCliente){
        if(tipoCliente==1)
            cliente.setTipoCliente(true);
        else if(tipoCliente==2)
            cliente.setTipoCliente(false);
        cliente.setNombre(nombre);
        cliente.setDni(dni);
        cliente.setDireccion(direccion);
    }
    // test
    public String toStringVerificar(){
        return "true";
    }
}
