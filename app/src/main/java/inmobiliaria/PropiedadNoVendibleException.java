package inmobiliaria;

public class PropiedadNoVendibleException extends RuntimeException
{
    public PropiedadNoVendibleException() {
        super("La propiedad no se puede vender, es un local");
    }
}