package inmobiliaria;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Factura {
    // atributos
    private Cliente cliente;
    private Propiedad propiedad;
    private BigDecimal precioVenta;
    private BigDecimal precioOpcional;
    private LocalDate fechaAlquilerInicio;
    private LocalDate fechaAlquilerFin;
    private LocalDate fechaComprobante;
    // constructor
    public Factura(Cliente cliente, Propiedad propiedad, BigDecimal precioVenta,BigDecimal precioOpcional, LocalDate fechaAlquilerInicio, LocalDate fechaAlquilerFin, LocalDate fechaComprobante){
        this.cliente=cliente;
        this.propiedad=propiedad;
        this.precioVenta=precioVenta;
        this.precioOpcional=precioOpcional;
        this.fechaAlquilerInicio=fechaAlquilerInicio;
        this.fechaAlquilerFin=fechaAlquilerFin;
        this.fechaComprobante=fechaComprobante;
    }
    //
    public BigDecimal getPrecioVenta(){
        return precioVenta;
    }
    public BigDecimal getPrecioOpcional(){
        return  precioOpcional;
    }
    public LocalDate getFechaAlquilerInicio(){
        return fechaAlquilerInicio;
    }
    public LocalDate getFechaAlquilerFin(){
        return fechaAlquilerFin;
    }
    public LocalDate getFechaComprobante(){
        return fechaComprobante;
    }
    //
    public Long calcularFechaAlquilerDias(){
        Long fechaAlquiler = (long) (fechaAlquilerFin.getDayOfMonth()-fechaAlquilerInicio.getDayOfMonth());
        return fechaAlquiler;
    }
    public Long calcularFechaAlquilerMensual(){
        Long fechaAlquiler = (long) (fechaAlquilerFin.getMonthValue()-fechaAlquilerInicio.getMonthValue());
        return fechaAlquiler;
    }
    public BigDecimal calcularPrecioAlquiler(){
        double precioTotal;
        Long diasDeAlquiler=calcularFechaAlquilerDias();
        Long mesesDeAlquiler=calcularFechaAlquilerMensual();
        BigDecimal precioMensual = propiedad.getPrecioAlquilerMensual();
        BigDecimal precioDiario = propiedad.getPrecioAlquilerDiario();
        precioTotal=precioMensual.longValue()*mesesDeAlquiler;
        precioTotal+=precioDiario.longValue()*diasDeAlquiler;
        BigDecimal precioCalculado = BigDecimal.valueOf(precioTotal);
        return precioCalculado;
    }
    public BigDecimal getCalcularPrecioTotal(){
        BigDecimal precioCalculado;
        if((calcularFechaAlquilerDias()>0)||(calcularFechaAlquilerMensual()>0))
        {
            precioCalculado=calcularPrecioAlquiler();
        }
        else
            precioCalculado=getPrecioVenta();
        return precioCalculado;
    }
    //
    public void mostrarFactura(){
        // cliente.mostrarCliente();
        propiedad.mostrarPropiedad();
        System.out.println("Fecha de comprobante: "+fechaComprobante);
        if((calcularFechaAlquilerDias()>0)||(calcularFechaAlquilerMensual()>0))
        {
            System.out.println("Fecha de inicio de alquiler: "+getFechaAlquilerInicio());
            System.out.println("Fecha de fin de alquiler: "+getFechaAlquilerFin());
            System.out.println("Precio de Alquiler"+calcularPrecioAlquiler());
        }
        else{
            System.out.println("Precio total: "+getPrecioVenta());
        }
    }
    // test
    public String toString(){
        return "true";
    }
}
