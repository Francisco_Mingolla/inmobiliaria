package inmobiliaria;
import java.math.BigDecimal;
public class Local extends Propiedad{
    private int baños;
    private int pisos;
    private int cocinas;
    private int m2vidriera;
    // constructor para test
    public Local(Integer metros2, String localidad, String barrio, String direccion, BigDecimal precioVenta, BigDecimal precioAlquilerMensual, int baños, int pisos, int cocinas, int m2vidriera){
        super(metros2, localidad, barrio, direccion, precioVenta, precioAlquilerMensual);
        this.baños=baños;
        this.pisos=pisos;
        this.cocinas=cocinas;
        this.m2vidriera=m2vidriera;
    }
    public BigDecimal getPrecioAlquilerDiario(){
        BigDecimal precio = BigDecimal.valueOf(0);
        return precio;
    }
    // test
    public String toString(){
        return "true";
    }
}
