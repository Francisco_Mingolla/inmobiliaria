package inmobiliaria;

import java.math.BigDecimal;

public abstract class Propiedad {
    // atributos
    protected Integer metros2;
    protected String localidad;
    protected String barrio;
    protected String direccion;
    protected BigDecimal precioVenta;
    protected BigDecimal precioAlquilerMensual;
    // constructor para test
    protected Propiedad(Integer metros2, String localidad, String barrio, String direccion, BigDecimal precioVenta, BigDecimal precioAlquilerMensual){
        this.metros2=metros2;
        this.localidad=localidad;
        this.barrio=barrio;
        this.direccion=direccion;
        this.precioVenta=precioVenta;
        this.precioAlquilerMensual=precioAlquilerMensual;
    }
    // mostrar propiedad
    public void mostrarPropiedad(){
        System.out.println("Informacion de la propiedad: ");
        System.out.println("Metros cuadrados: "+metros2);
        System.out.println("Localidad: "+localidad);
        System.out.println("Barrio:"+barrio);
    }
    // gets
    public BigDecimal getPrecioVenta(){
        return precioVenta;
    }
    public BigDecimal getPrecioAlquilerMensual(){
        return precioAlquilerMensual;
    }
    public abstract BigDecimal getPrecioAlquilerDiario();
    public Integer getM2(){
        return metros2;
    }
}

